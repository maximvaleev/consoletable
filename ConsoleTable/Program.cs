﻿/*
 * Домашнее задание
Таблица в консоли

Цель:
В этом задании мы закрепим знания, полученные в ходе вебинара, и сможем воспользоваться циклами и условными операторами

Описание/Пошаговая инструкция выполнения домашнего задания:
Нужно написать программу, которое делает следующее:
1   Вывести текст введите размерность таблицы:
    после этого считать вводимую строку от пользователя в виде целого числа,
    если введенная строка не соответствует формату целого числа (не парсится),
    то нужно повторно вывести текст введите размерность таблицы: , и выводить
    его до тех пор, пользователь не введет корректное число.
    Число должно быть не меньше 1, и не больше 6. Обозначим его как n.
2   Вывести текст введите произвольный текст: ,
    если пользователь введет пустой текст, снова выводим введите произвольный текст: ,
    до тех пор, пока пользователь не введет непустую строку.
3   Нужно вывести таблицу, у которой будут следующие свойства
*        Ее ширина не должна превышать 40 символов
*        Границы таблицы - символ +
*        Ширина таблицы (каждой строки) зависит от числа n и длины введенной строки из п.2.
*        Вывести 1ю строку таблицы с текстом, введенным в п.2., который находится на расстоянии n-1 от каждой из границ строки.
*        Вывести 2ю строку таблицы. Она имеет ту же высоту, что и строка 1, и представляет собой набор символов +, чередующихся в шахматном порядке.
*        Вывести 3ю строку таблицы. Она должна быть квадратной, "перечеркнутая" символом + по диагоналям
*        В программе должны использоваться циклы do while, while и for и ?:


Критерии оценки:

2 балла - за каждую строку (итого 6)
1 балл - если горизонтальная граница таблицы, будет собрана один раз, а выводится будет одной строкой
1 балл, если построение каждой строки будет обернуто в отдельную функцию
2 балла, если построение всей таблицы будет проходить в рамках одного цикла, а выбор, какую строку строить - при помощи switch case when
Для зачета достаточно набрать 6 баллов

Рекомендуем сдать до: 23.05.2023
*/


using System.Runtime.Versioning;
using System.Text;

namespace ConsoleTable
{
    internal partial class Program
    {
        // последовательность ячеек в таблице
        static CellType[] _cellSequence = { CellType.Text, CellType.Checker, CellType.Cross };
        const char BorderChar = '+';
        const char ContentChar = '*';
        const int MaxPadding = 6; // значения больше 9 не поддерживаются.
        const int MaxCellSize = 40;

        static string _borderLine, _emptyLine;
        // размеры ячеек включают обе окружающие их границы
        static int _cellWidth, _cellHeight;


        static void Main(string[] args)
        {
            Console.WriteLine("Hello, Friend! Let's draw a table.\n");
            // получить от пользователя отступ от края ячейки до текста (паддинг включает границу таблицы)
            int padding = GetUserPadding();
            // получить от пользователя текст в ячейке
            string userText = GetUserText(padding);
            // рассчитать размеры ячейки
            _cellWidth = userText.Length + 2 * padding;
            _cellHeight = 2 * padding + 1;
            // построить горизонтальную границу и пустую строку
            ConstructStandardStrings();

            // добавить немного пафоса (Только для Windows)
            //WhipUpPathos();

            Console.WriteLine("\n\n" + _borderLine);
            // построение ячеек в указанном порядке
            foreach (CellType cellType in _cellSequence)
            {
                switch (cellType)
                {
                    case CellType.Text:
                        DrawTextCell(userText, padding);
                        break;
                    case CellType.Checker:
                        DrawCheckerCell();
                        break;
                    case CellType.Cross:
                        DrawCrossCell();
                        break;
                    default:
                        throw new NotImplementedException("There's no implementation for cell type " + cellType);
                }
                Console.WriteLine(_borderLine);
            }
            Console.WriteLine();
        }

        #region UserDataRequests

        /// <summary>
        /// Получить от пользователя отступ от текста до краев в формате int
        /// </summary>
        private static int GetUserPadding()
        {
            char userInput;
            int digit;

            Console.Write("Enter cell padding (1 to " + MaxPadding + "): ");
            
            do
            {
                // отображение вводимых символов подавляется, пока не будет введена подходящая цифра
                userInput = Console.ReadKey(true).KeyChar;
                digit = Char.IsDigit(userInput) ? (int)Char.GetNumericValue(userInput) : 0;
            } while (digit == 0 || digit > MaxPadding);
            Console.WriteLine(userInput);

            return digit;
        }

        /// <summary>
        /// Получить от пользователя текст - хотябы 1 непробельный символ
        /// </summary>
        private static string GetUserText(int padding)
        {
            Console.Write("\nNice! Now, please write some text: ");
            
            string? userInput = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(userInput))
            {
                Console.Write("C'mon, friend, type something: ");
                userInput = Console.ReadLine();
            }
            // если строка слишком длинная, то ограничить ее длину
            if (userInput.Length + 2 * padding > MaxCellSize)
                userInput = userInput.Substring(0, MaxCellSize - 2 * padding);

            return userInput;
        }

        #endregion

        /// <summary>
        /// Создать строку заполненную символом границы целиком и строку с пробелами с границами по бокам
        /// </summary>
        private static void ConstructStandardStrings()
        {
            _borderLine = new string(BorderChar, _cellWidth);
            _emptyLine = BorderChar + new string(' ', _cellWidth - 2) + BorderChar;
        }

        #region DrawingTableCells

        /// <summary>
        /// Заполнить ячейку с текстом
        /// </summary>
        private static void DrawTextCell(string userText, int padding)
        {
            for (int row = 1; row < _cellHeight - 1; row++)
            {
                if (row != padding)
                    Console.WriteLine(_emptyLine);
                else
                {
                    string spaces = new(' ', padding - 1);
                    Console.WriteLine(BorderChar + spaces + userText + spaces + BorderChar);
                }
            }
        }

        /// <summary>
        /// Заполнить ячейку с шашечками
        /// </summary>
        private static void DrawCheckerCell()
        {
            StringBuilder strBuilder = new(_cellWidth);
            var checkerLines = new string[2];
            // построить четную и нечетную линии
            for (int i = 0; i < checkerLines.Length; i++)
            {
                strBuilder.Append(BorderChar);
                for (int j = 1; j < _cellWidth - 1; j++)
                {
                    _ = (j + i) % 2 == 0 ? strBuilder.Append(ContentChar) : strBuilder.Append(' ');
                }
                strBuilder.Append(BorderChar);

                checkerLines[i] = strBuilder.ToString();
                strBuilder.Clear();
            }
            // Вывести линии в консоль с чередованием.
            for (int i = 1; i < _cellHeight - 1; i++)
                Console.WriteLine(checkerLines[i % 2]);
        }

        /// <summary>
        /// Заполнить ячейку с крестом
        /// </summary>
        private static void DrawCrossCell()
        {
            StringBuilder strBuilder = new(_cellWidth);

            for (int row = 1; row < _cellWidth - 1; row++)
            {
                strBuilder.Append(BorderChar);
                for (int col = 1; col < _cellWidth - 1; col++)
                {
                    if (col == row || col == _cellWidth - 1 - row)
                        strBuilder.Append(ContentChar);
                    else
                        strBuilder.Append(' ');
                }
                strBuilder.Append(BorderChar);

                Console.WriteLine(strBuilder.ToString());
                strBuilder.Clear();
            }
        }

        #endregion  

        /// <summary>
        /// Добавляет немного "спецэффектов" (только для Windows)
        /// </summary>
        [SupportedOSPlatform("windows")]
        private static void WhipUpPathos()
        {
            Console.Beep(600, 200);
            Thread.Sleep(1000);
            Console.Clear();
            Thread.Sleep(800);
            Console.Beep(500, 200);
            Console.WriteLine("\nat last we are ready... BEHOLD THE TABLE!!!");
            Thread.Sleep(1700);
            Console.Clear();
            Console.Beep(300, 1500);
        }
    }
}